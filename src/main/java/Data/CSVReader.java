package Data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class CSVReader extends dataSekolah {

    public void readCSV(String path) {
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));

            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                addKelas(values[0]);
                ArrayList<Integer> dataValues = new ArrayList<Integer>();
                for (int i = 1; i < values.length; i++) {
                    addValues(Integer.parseInt(values[i]));

                    dataValues.add(Integer.parseInt(values[i]));
                }
                addValuesPerclass(values[0], dataValues);
            }
            System.out.println("File telah diload");
        } catch (FileNotFoundException fne) {
            System.out.println("File does not exist please try again. " + fne.getMessage());
        } catch (IOException ioe) {
            System.out.println("IO Error " + ioe.getMessage());
        } catch (NullPointerException nul){
            System.out.println("Anda tidak menginput apapun " + nul.getMessage());
        }
    }



    public ArrayList<Integer> getValues() {
        ArrayList<Integer> values = this.values;
        Collections.sort(values);
        return values;
    }

    public ArrayList<String> getKelas() {
        return this.kelas;
    }

    public ArrayList<Integer> getValuesbyClass(String kelas) {
        ArrayList<Integer> values = this.valuesPerclass.get(kelas);
        Collections.sort(values);
        return values;
    }

}
