package GenerateFile;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Statistics extends processingData {


    @Override
    public float Mean(ArrayList<Integer> list) {
        float mean;
        int sum = 0;


        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
        return sum / list.size();
    }

    @Override
    public double Median(ArrayList<Integer> list) {
        int middle = list.size() / 2;
        if (list.size() % 2 == 1) {
            return list.get(middle);
        } else {
            return (list.get(middle - 1) + list.get(middle)) / 2.0;
        }
    }

    @Override
    public float Mode(ArrayList<Integer> listValues) {
        int maxValue = 0, maxCount = 0;

        for (int i = 0; i < listValues.size(); ++i) {
            int count = 0;
            for (int j = 0; j < listValues.size(); ++j) {
                if (listValues.get(j) == listValues.get(i)) ++count;
            }
            if (count > maxCount) {
                maxCount = count;
                maxValue = listValues.get(i);
            }
        }

        return maxValue;
    }


    public String generateData(ArrayList<Integer> dataList) {
        float mean = Mean(dataList);
        double median = Median(dataList);
        float mode = Mode(dataList);

        String statement = "--------------------------------------\n";
        statement += "HASIL DARI PERHITUNGAN STATISTIK\n";
        statement += "--------------------------------------\n";
        statement += "Mean : " + mean + "\n";
        statement += "Median : " + median + "\n";
        statement += "Modus : " + mode + "\n";
        statement += "--------------------------------------";

        return statement;
    }

    public String generateData(ArrayList<Integer> dataList, String Kelas) {
        float mean = Mean(dataList);
        double median = Median(dataList);
        float mode = Mode(dataList);

        String statement = "--------------------------------------\n";
        statement += "HASIL DARI PERHITUNGAN STATISTIK DARI " + Kelas + "\n";
        statement += "--------------------------------------\n";
        statement += "Mean : " + mean + "\n";
        statement += "Median : " + median + "\n";
        statement += "Modus : " + mode + "\n";
        statement += "--------------------------------------";

        return statement.toString();
    }


    @Override
    public void writeTxt(String filePath, String statement) {
        try {
            File file = new File(filePath);
            file.exists();
            if (file.createNewFile()) {
                System.out.println("New file has been created!");
            } else {
                System.out.println("Rewriting file in " + filePath);
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(writer);
            bw.write(statement);
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
